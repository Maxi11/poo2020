
public class Main { //Por el momento esta visibilidad publica

	public static void main(String[] args) {
		System.out.println("Hola mundo");
		//TIPO PRIMITIVO DE DATOS
		int NumEntero = 34; 
		double NumDec = 45.6; 
		boolean UnBooleano = true;
		char carac = 'Z';
		int soloEntero = (int) 5.6; //Solo toma el numero entero
		
		
		/*
		 * Comentario de varias lineas
		 * operadores logicos
		 * & = y condicional and
		 * && = y evalua la primera condicion si es falsa no evalua la segunda
		 * | = o condicional or
		 *  || = o evalua la primera condicion si es verdadera no evalua la segunda
		 */
		// Un if corto
		System.out.println(NumEntero < NumDec? "El numero entero es menor que el decimal" : "El numero entero es mayor que el decumal");
		// Un switch
		switch (carac) {
		case 'A':
			System.out.println("Es el caracter A");
			break;//importante siempre usar el break
		case 'B':
			System.out.println("Es el caracter B");
			break;
		case 'Z':
			System.out.println("Es el caracter Z");
			break;
		default:
			break;
		}
		//Arreglos estaticos
		int[] vectorNum = {1,2,3,4,5};
		
		//Bucle while
		int i=0;
		while (i< vectorNum.length) {
			System.out.println(vectorNum[i]);
			i++;
		} 
		//Bucle do
		int j=4;
		do {
			vectorNum[j]=j;
			System.out.println("Numero con el bucle do "+vectorNum[j]);
			j--;
		} while (j>=0);
	
	//Bucle for
	for (int k = 0; k < vectorNum.length; k++) {
		System.out.println("Bucle for = "+vectorNum[k]);
	}
	//operadores compuestos
	i+=10; //Es igual a i = i + 10
	i-=8; //Es igual a i = i-8
	i/=2; // Es igual a i = i / 2
	i*=5; // Es igual a i = i * 5
	i%=2; //Es igual a i = i mod 2
	
	++i; //Incrementa el valor y lo utiliza
	i++; //Utiliza el valor y despues lo incrementa
	
	}
}

